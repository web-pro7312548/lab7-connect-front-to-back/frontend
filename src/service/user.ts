import http from './http'
import type { User } from '@/types/User'

function addUser(user: User) {
  return http.post('/users', user)
}

function updateUser(user: User) {
  return http.patch(`/users/${user.id}`, user)
}

function delUser(user: User) {
  return http.delete(`/users/${user.id}`)
}

function getUser(id: number) {
  return http.get(`/users/${id}`)
}

function getUsers() {
  return http.get('/users')
}

export default { addUser, updateUser, delUser, getUser, getUsers }
