import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useloadingStore = defineStore('loading', () => {
  const isLoading = ref(false)
  const doload = () => {
    isLoading.value = true
  }
  const finish = () => {
    isLoading.value = false
  }

  return { isLoading, doload, finish }
})
