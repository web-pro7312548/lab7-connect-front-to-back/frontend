import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import http from '@/service/http'
import { useloadingStore } from './loading'
import temperatureService from '@/service/temperature'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loadingStore = useloadingStore()

  async function callConvert() {
    console.log('Store: call Convert')

    // result.value = convert(celsius.value)
    loadingStore.doload()
    try {
      result.value = await temperatureService.convert(celsius.value)
    } catch (e) {
      console.log(e)
    }
    loadingStore.finish()
    console.log('Store: Finish call Convert')
    console.log(result.value)
  }
  return { valid, result, celsius, callConvert }
})
