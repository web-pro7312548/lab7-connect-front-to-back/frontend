import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useloadingStore } from './loading'
import userService from '@/service/user'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
  const loadingStore = useloadingStore()
  const users = ref<User[]>([])

  async function getUser(id: number) {
    loadingStore.doload()
    const res = await userService.getUser(id)
    users.value = res.data
    console.log(res.data)
    loadingStore.finish()
  }
  async function getUsers() {
    loadingStore.doload()
    const res = await userService.getUsers()
    users.value = res.data
    console.log(res.data)
    loadingStore.finish()
  }
  async function saveUser(user: User) {
    if (user.id < 0) {
      const res = await userService.addUser(user)
    } else {
      const res = await userService.updateUser(user)
    }
    await getUsers() //wait finish
    loadingStore.finish()
  }

  async function deleteUser(user: User) {
    loadingStore.doload()
    const res = await userService.delUser(user)
    await getUsers()
    loadingStore.finish()
  }
  return { users, getUsers, saveUser, deleteUser, getUser }
})
